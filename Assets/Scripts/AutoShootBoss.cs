﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoShootBoss : MonoBehaviour
{
    public Transform bulletsPositionTransform;
    public GameObject bulletsBossPrefab;
    public float repeateRate = 1f;
    private float newReateRate;
    public static bool isHalfLife;

    public float fireRate = 0.5f;
    private float nextFire = 0;
    private bool azerty;

    void Start()
    {
        newReateRate = repeateRate / 2;
        isHalfLife = false;
        azerty = true;
        InvokeRepeating("SpawnBullet", 3.0f, repeateRate);
    }

    private void Update()
    {
        if (isHalfLife)
        {
            if (azerty)
            {
                Debug.Log("CancelInvoke(SpawnBullet)");
                CancelInvoke("SpawnBullet");
                azerty = false;
            }
            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
                Invoke("SpawnBullet", newReateRate);
            }
        }
    }

    void SpawnBullet()
    {
        FindObjectOfType<AudioManager>().Play("ShootBoss");
        Instantiate(bulletsBossPrefab, bulletsPositionTransform.position, bulletsPositionTransform.rotation);
    }
}
