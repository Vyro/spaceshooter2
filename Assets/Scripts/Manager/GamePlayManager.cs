﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class GamePlayManager : MonoBehaviour
{
    public static GamePlayManager Instance;
    public bool startGame;
    bool gameHasEnded = false;
    public GameObject gameOver;

    static public Text scoreText;
    public Text scoreGameOver;
    static public int score;
    private readonly string scoreTag = "ScoreText";
    public static bool isWait = false;
    public static bool goMenu = false;
    private float count = 10f;

    public static GameObject CompleteLevelUI;

    void Start()
    {
        gameOver.SetActive(false);
        scoreText = GameObject.FindGameObjectWithTag(scoreTag).GetComponent<Text>();
        CompleteLevelUI = GameObject.FindGameObjectWithTag("LevelComplete");
        score = 0;
        scoreText.text = score.ToString();
    }


    void Update()
    {
        if(scoreText != null)
        {
            scoreText.text = score.ToString();
        }

        /*if (isWait)
        {
            count -= Time.deltaTime;
            if(count <= 0)
            {
                goMenu = true;
                count = 50;
            }
        }*/
    }

    public void LevelToLoad(int level)
    {
        SceneManager.LoadScene(level);
    }


    // Méthode appelée dans Player
    public void EndGame()
    {
        if(gameHasEnded == false)
        {
            gameHasEnded = true;
            gameOver.SetActive(true);
            Time.timeScale = 1f;
            scoreGameOver.text = "Score: " + score;
            score = 0;
        }
    }
    // Les méthodes pour redémarrer la partie ou quitter le jeu sont appelées depuis la fenêtre du "game over"
    // Redémarre le jeu
    public void Restartou()
    {
        score = 0;
        Debug.Log("Restart()");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Redémarre la scène 01
    public void StarGame()
    {
        score = 0;
        SceneManager.LoadScene("Level01");
    }

    // Quitte le jeu
    public void ReturnToMenu()
    {
        //  SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        SceneManager.LoadScene(0);
    }

    public static void CompleteLevel()
    {
        Debug.Log("LEVEL WON !");
        if(CompleteLevelUI != null)
        {
            CompleteLevelUI.SetActive(true);
        }
        else
        {
            CompleteLevelUI = GameObject.FindGameObjectWithTag("LevelComplete");
            Debug.Log("CompleteLevelUI: " + CompleteLevelUI);
            if(CompleteLevelUI != null)
            {
                CompleteLevelUI.SetActive(true);
            }
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        /*if ((SceneManager.GetActiveScene().buildIndex + 1) == 3)
        {
            Time.timeScale = 1f;
            isWait = true;
            if (goMenu)
            {
                SceneManager.LoadScene(0);
            }
        }*/

    }


    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else if(Instance != null)
        {
            Destroy(gameObject);
        }
    }

}
