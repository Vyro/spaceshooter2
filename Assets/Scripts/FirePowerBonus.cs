﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePowerBonus : MonoBehaviour
{
    public float speed = 5f;
    public Rigidbody2D rb2D;

    void Start()
    {
        rb2D.velocity = -transform.up * speed;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            ShootAction.isGetBonusFireBonus = true;
            FindObjectOfType<AudioManager>().Play("CatchFireBonus");
            Destroy(this.gameObject);
        }
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
