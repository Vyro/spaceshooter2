﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullets : MonoBehaviour
{
    public float speed = 5f;
    public Rigidbody2D rb2D;
    public bool goDown = false;

    void Start()
    {
        if (!goDown)
        {
            rb2D.velocity = transform.up * speed;
        }
        else
        {
            rb2D.velocity = -transform.up * speed;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        // Fait disparaitre les tirs quand l'ennemi est touché
        if (collision.gameObject.tag == "Enemy")
        {
            FindObjectOfType<AudioManager>().Play("EnemyDeath");
            Destroy(this.gameObject);
        }

        if (collision.gameObject.tag == "Boss")
        {
            Destroy(this.gameObject);
        }
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

}
