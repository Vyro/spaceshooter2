﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossApparition : MonoBehaviour
{
    public Transform transformSpawn;
    public GameObject boss;
    
    public int scoreAttendu;
    private int limitSpawn = 0;

    void Start()
    {
        //scoreAttendu = 500;
    }    

    void Update()
    {       

        if (GamePlayManager.score >= scoreAttendu)
        {
            // Arrête de faire apparaitre les ennemis
            Spawn.waveEnemies = false;
            // Faire apparaitre le boss
            limitSpawn += 1;
            if (limitSpawn == 1)
            {
                Debug.Log("Le boss arrive !");
                Instantiate(boss, transformSpawn.position, transformSpawn.rotation);
            }

        }
    }

    
}
