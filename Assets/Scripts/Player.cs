﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Camera MainCamera;
    private Vector2 screenBounds;
    public float speed;

    private float objectWidth;
    private float objectHeight;

    public static int healPoint;
    public Text HPText;

    void Start()
    {
        healPoint = 3;
        HPText.text = "❤❤❤";
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "FireBulletsEnemy" 
            || collision.gameObject.tag == "BulletsBoss")
        {
            GetDamage();
            if (healPoint <= 0)
            {
                // Music mort du Player
                FindObjectOfType<AudioManager>().Play("DeathFail");
                // Destruction du Player
                Destroy(this.gameObject);
                // Appel la méthode EndGame() depuis la classe GamePlayManager.cs
                FindObjectOfType<GamePlayManager>().EndGame();
            }
        }

        if (collision.gameObject.tag == "Heal")
        {
            GetHealth();
        }

    }

    public void GetDamage()
    {
        if (healPoint > 0)
        {
            healPoint -= 1;
            SetHpText();
        }
    }

    public void GetHealth()
    {
        healPoint += 1;
        SetHpText();
    }

    void SetHpText()
    {
        switch (healPoint)
        {
            case 0:
                {
                    HPText.text = "Dead";
                    break;
                }
            case 1:
                {
                    HPText.text = "❤";
                    break;
                }
            case 2:
                {
                    HPText.text = "❤❤";
                    break;
                }
            case 3:
                {
                    HPText.text = "❤❤❤";
                    break;
                }
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.down * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
    }

    void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x * -1 + objectWidth, screenBounds.x - objectWidth);
        viewPos.y = Mathf.Clamp(viewPos.y, screenBounds.y * -1 + objectWidth, screenBounds.y - objectHeight);
        transform.position = viewPos;
    }

}
