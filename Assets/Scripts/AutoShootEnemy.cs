﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoShootEnemy : MonoBehaviour
{
    // public Transform firePointTransform;
    public Transform enemyTransform;
    public GameObject fireBulletsPrefab;

    private void Start()
    {
        InvokeRepeating("SpawnBullet", 0.1f, 1.0f);
    }


    void SpawnBullet()
    {
        Instantiate(fireBulletsPrefab, enemyTransform.position, enemyTransform.rotation);
    }

    
}
