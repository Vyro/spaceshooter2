﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBulletsEnemy : MonoBehaviour
{
    public float speed = 5f;
    public Rigidbody2D rb2D;
    public bool goDown = false;

    void Start()
    {
        if (!goDown)
        {
            rb2D.velocity = transform.up * speed;
        }
        else
        {
            rb2D.velocity = -transform.up * speed;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            FindObjectOfType<AudioManager>().Play("Damage");
            Destroy(gameObject);
        }
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
