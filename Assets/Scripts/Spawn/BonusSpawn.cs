﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpawn : MonoBehaviour
{
    public Transform transform;
    private float largeur;
    public GameObject healPrefab;
    public GameObject firePowerPrefab;
    private Vector3[] positions;
    private bool needHeal;

    public float timerFirePower = 30;
    public float timerHeal = 5;

    void Start()
    {
        needHeal = false;
        if (transform != null)
        {
            largeur = transform.localScale.x;
            positions = new Vector3[(int)largeur];
        }

        for (int i = 0; i < largeur - 1; i++)
        {
            Vector3 position = new Vector3((largeur/2) - i, transform.position.y);
            positions.SetValue(position, i);
        }
    }

    private void Update()
    {
        if(Player.healPoint == 1)
        {
            needHeal = true;
        }

        // Apparition du bonus heal
        if(needHeal)
        {
            timerHeal -= Time.deltaTime;
            if(timerHeal <= 0)
            {
                timerHeal = 6;
                needHeal = false;
                Instantiate(
                healPrefab,
                positions[Random.Range(0, positions.Length)],
                transform.rotation);
            }
        }

        timerFirePower -= Time.deltaTime;
        if(timerFirePower <= 0)
        {
            timerFirePower = 30;
            Instantiate(firePowerPrefab, positions[Random.Range(0, positions.Length)], transform.rotation);
        }
    }
}
