﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public Transform transform;
    private float hauteur;
    public GameObject enemyPrefab;
    private Vector3[] positions;
    public static bool waveEnemies;

    void Start()
    {
        waveEnemies = true;
        if (transform != null)
        {
            hauteur = transform.localScale.y;
            positions = new Vector3[(int) hauteur];
        }

        for(int i = 0; i < hauteur - 1; i++)
        {
            Vector3 position = new Vector3(transform.position.x, (hauteur/2) - i);
            positions.SetValue(position, i);
        }
        
        InvokeRepeating("SpawnEnemy", 0.1f, 1.0f);
    }

    void Update()
    {
        if (waveEnemies == false)
        {
            StopSpawnEnemy();
        }
    }

    void SpawnEnemy()
    {
        if (waveEnemies)
        {
            Instantiate(
                enemyPrefab, 
                positions[Random.Range(0, positions.Length)], 
                transform.rotation);
        }
    }

    void StopSpawnEnemy()
    {
        CancelInvoke("SpawnEnemy");
    }
}
