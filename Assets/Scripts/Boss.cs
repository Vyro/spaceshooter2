﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Boss : MonoBehaviour
{

    public float speed = 4f;
    public Rigidbody2D rb2D;
    public int pv = 20;
    private float halfLife;
    public GameObject boss;
    private float direction;
    private Vector2 Offset;
    private GameObject StopPositionBoss;
    private bool canMoveLeftRight;
    public static bool bossIsDead;

    public GamePlayManager gameManager;

    void Start()
    {
        bossIsDead = false;
        halfLife = pv / 2;
        canMoveLeftRight = false;
        Offset = new Vector2(-3, 3);
        direction = 1;
        Invoke("Deplacement", 0.5f);
        StopPositionBoss = GameObject.FindGameObjectWithTag("StopPositionBoss");
    }


    void Update()
    {
        if (canMoveLeftRight)
        {
            DeplacementGaucheDroite();
        }
        DeathBoss();
    }

    void DeplacementGaucheDroite()
    {
        if(transform.position.x > Offset.y)
        {
            direction = -1f;
        }
        else if(transform.position.x < Offset.x)
        {
            direction = 1f;
        }

        transform.position = transform.position + new Vector3(speed * direction * Time.deltaTime, 0, 0);
    }

    void Deplacement()
    {
        rb2D.velocity = -transform.up * speed;
    }

    void BossGetDamage()
    {
        pv -= 1;
        if(pv == halfLife)
        {
            Debug.Log("halfLife");
            AutoShootBoss.isHalfLife = true;
            Offset.x = -6;
            Offset.y = 6;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "StopPositionBoss")
        {
            CancelInvoke("Deplacement");
            rb2D.velocity = Vector2.zero;
            Destroy(StopPositionBoss);
            canMoveLeftRight = true;
        }

        if (collision.gameObject.tag == "FireBulletsPlayer")
        {
            FindObjectOfType<AudioManager>().Play("Damage");
            BossGetDamage();
        }
    }

    void DeathBoss()
    {
        if (pv <= 0)
        {
            GamePlayManager.score += 100;
            FindObjectOfType<AudioManager>().Play("DeathBoss");
            Destroy(boss);
            FindObjectOfType<AudioManager>().Play("LevelCompleted");
            GamePlayManager.CompleteLevel();
        }
    }    

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
