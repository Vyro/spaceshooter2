﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootAction : MonoBehaviour
{
    public Transform firePointTransform;
    public GameObject fireBulletsPrefab;

    public float fireRate = 1.0f;
    private float nextFire = 0;
    private int delayShootAuto = 0;

    public static bool isGetBonusFireBonus;
    public float delayBonus;

    private void Start()
    {
        delayBonus = 5.0f;
    }

    void Update()
    {
        if (isGetBonusFireBonus)
        {
            if (delayShootAuto == 5)
            {
                Invoke("Shoot", 0.5f);
                delayShootAuto = 0;
            }
            else
            {
                delayShootAuto += 1;
            }
            delayBonus -= Time.deltaTime;
        }
        else if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Shoot();
        }

        if (delayBonus <= 0)
        {
            isGetBonusFireBonus = false;
            CancelInvoke("Shoot");
            delayBonus = 5.0f;
        }


    }

    public void BonusFireRate(float bonusTimeFire)
    {
        while (bonusTimeFire > 0)
        {
            // Tire rapidement plein de balles !!!!
            InvokeRepeating("Shoot", 0.1f, 0.1f);
            bonusTimeFire -= Time.deltaTime;
            Debug.Log(bonusTimeFire);
        }

    }

    void Shoot()
    {
        FindObjectOfType<AudioManager>().Play("Shoot");
        Instantiate(fireBulletsPrefab, firePointTransform.position, firePointTransform.rotation);
    }

    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
