﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PercutionBullets : MonoBehaviour
{
    private int score;
    void Start()
    {
        score = 0;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        // Fait disparaitre l'ennemi quand celui-ci est touché
        if (collision.gameObject.tag == "FireBulletsPlayer")
        {
            Destroy(this.gameObject);
            score +=  10;
            GamePlayManager.score += score;
        }
    }

    void Update()
    {

    }
}
