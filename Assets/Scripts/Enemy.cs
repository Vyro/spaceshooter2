﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    public Transform enemyTransform;
    public float[] speeds;

    void Start()
    {
        speeds = new float[3];
        speeds.SetValue(2, 0);
        speeds.SetValue(4, 1);
        speeds.SetValue(6, 2);

        speed = speeds[Random.Range(0, speeds.Length)];
    }

    void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }

    public Transform GetTransform()
    {
        return enemyTransform;
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

}
